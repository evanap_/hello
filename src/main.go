package main

import (
	"log"
	"net/http"
	"strconv"
)

var (
	port       = ":8080"
)

func main() {

	v := 0
	http.HandleFunc("/healthz", func(w http.ResponseWriter, _ *http.Request) {
		v += 1
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(strconv.Itoa(v)))
	})

	log.Print("Starting server on port ", port)
	log.Fatal(http.ListenAndServe(port, nil))

}

